FROM alpine:3.7
MAINTAINER Martin Espinach <martin.espinach@jamgo.coop>

ENV OPENLDAP_VERSION=2.4.46

RUN mkdir -vp \
        /etc/openldap/sasl2 \
        /srv/openldap.d \
        /srv/custom.d \
        /tmp/openldap \
        /var/run/openldap \
 && export BUILD_DEPS=" \
        autoconf \
        automake \
        curl \
        cyrus-sasl-dev \
        db-dev \
        g++ \
        gcc \
        groff \
        gzip \
        libtool \
        make \
        mosquitto-dev \
        openssl-dev \
        tar \
        unixodbc-dev \
        util-linux-dev \
    " \
 && apk add --update \
        gettext \
        libintl \
        openldap \
        unixodbc \
        openssl \
        ${BUILD_DEPS} \
# Grab envsubst from gettext
 && cp -v /usr/bin/envsubst /usr/local/bin/ \
# Install OpenLDAP from source
 && curl -fL ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/openldap-${OPENLDAP_VERSION}.tgz -o /tmp/openldap.tgz \
# && curl -fL http://git.alpinelinux.org/cgit/aports/plain/main/openldap/configs.patch?h=3.7-stable -o /tmp/configs.patch \
# && curl -fL http://git.alpinelinux.org/cgit/aports/plain/main/openldap/openldap-2.4.11-libldap_r.patch?h=3.7-stable -o /tmp/openldap-2.4.11-libldap_r.patch \
 && tar -xzf /tmp/openldap.tgz --strip=1 -C /tmp/openldap \
 && cd /tmp/openldap \
# && for p in /tmp/*.patch; do patch -p1 -i $p || true; done \
 && rm -vrf /etc/openldap/schema /usr/sbin/slap* /usr/lib/slap* \
 && ./configure \
        --prefix=/usr \
        --libexecdir=/usr/lib \
        --sysconfdir=/etc \
        --mandir=/tmp/man \
        --localstatedir=/var/lib/openldap \
        --enable-crypt \
        --enable-dynamic \
        --enable-modules \
        --enable-local \
        --enable-slapd \
        --enable-spasswd \
        --enable-bdb=mod \
        --enable-hdb=mod \
        --enable-dnssrv=mod \
        --enable-ldap=mod \
        --enable-meta=mod \
        --enable-monitor=mod \
        --enable-null=mod \
        --enable-passwd=mod \
        --enable-relay=mod \
        --enable-shell=mod \
        --enable-sock=mod \
        --enable-sql=mod \
        --enable-overlays=mod \
        --with-tls=openssl \
        --with-cyrus-sasl \
 && make \
 && make install \
 && cd contrib/slapd-modules/passwd/sha2 \
 && make prefix=/usr libexecdir=/usr/lib \
 && make prefix=/usr libexecdir=/usr/lib install \
 && cd /usr/sbin && ln -vs ../lib/slapd \
 && chown -vR ldap:ldap \
        /etc/openldap \
        /var/lib/openldap \
        /var/run/openldap \
 && chmod +x /usr/lib/openldap/pw-sha2* \
 && apk del --purge \
        gettext \
        ${BUILD_DEPS} \
 && mv -vf /etc/openldap/ldap.conf /etc/openldap/ldap.conf.original \
 && mv -vf /etc/openldap/slapd.conf /etc/openldap/slapd.conf.original \
 && echo "mech_list: plain external" > /etc/openldap/sasl2/slapd.conf \
 && rm -vfr \
        /tmp/* \
        /usr/share/man/* \
        /var/tmp/* \
        /var/cache/apk/*

COPY *.template /srv/openldap/
COPY openldap.sh /srv/
COPY env-secrets-expand.sh /srv/

RUN set -x \
 && chmod -v +x /srv/openldap.sh \
 && chmod -v +x /srv/env-secrets-expand.sh

VOLUME ["/etc/openldap/slapd.d", "/var/lib/openldap"]

EXPOSE 389

ENTRYPOINT ["/srv/openldap.sh"]
